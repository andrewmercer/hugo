---
menu: main
title: Technical
---

# Certifications

## Currently held and in progress certifications

### [Kubernetes](https://kubernetes.io/training)

* [CKA: Certified Kubernetes Administrator](https://www.credly.com/badges/e58a6b4c-913a-4ff0-9927-772454f7062e/public_url)
* [CKAD: Certified Kubernetes Application Developer](https://www.credly.com/badges/9d6535bb-d6ed-4e39-8ee2-fe3512d9c2a9/public_url)
* [CKS: Certified Kubernetes Security Specialist](https://training.linuxfoundation.org/certification/certified-kubernetes-security-specialist) - in progress

### [Hashicorp](https://www.hashicorp.com/certification)

* [HashiCorp Certified: Terraform Associate (003)](https://www.credly.com/badges/df19c2be-db87-4f0a-84c5-484156bbfce4)
* [HashiCorp Certified: Vault Associate (002)](https://www.hashicorp.com/certification/vault-associate) - in progress
* [HashiCorp Certified: Consul Associate (002)](https://www.hashicorp.com/certification/consul-associate) - in progress
* [HashiCorp Certified: Vault Operations Professional](https://www.hashicorp.com/certification/vault-operations-professional) - in progress

### [Mirantis](https://training.mirantis.com/certification)

* [DCA: Docker Certified Associate](https://training.mirantis.com/certification/dca-certification-exam/) - in progres
* [Mirantis Kubernetes Engine (MKE)](https://training.mirantis.com/courses/cn212-mirantis-kubernetes-engine-mke) - in consideration
* [Mirantis Secure Registry (MSR)](https://training.mirantis.com/courses/cn213-mirantis-secure-registry) - in consideration
* [Mirantis OpenStack (MOS) For Kubernetes](https://training.mirantis.com/courses/mos210-mirantis-openstack-kubernetes-operations) - in consideration

## Previous held certifications
* [Red Hat Certified Engineer (RHCE)](https://www.redhat.com/en/services/certification/rhce)
* [AWS Certified Solutions Architect - Associate](https://aws.amazon.com/certification/certified-solutions-architect-associate)

# Code Samples

* [Ansible code samples](https://gitlab.com/andrewmercer/public/-/tree/main/ansible)
